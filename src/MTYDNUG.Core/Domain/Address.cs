﻿namespace MTYDNUG.Core.Domain
{
    using Enumerations;

    public class Address
    {
        public int Id { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public State State { get; set; }

        public string ZipCode { get; set; }
    }
}