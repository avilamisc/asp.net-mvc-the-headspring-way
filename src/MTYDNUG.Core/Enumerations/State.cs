﻿namespace MTYDNUG.Core.Enumerations
{
    using Headspring;

    public class State : Enumeration<State>
    {
        public static readonly State Alabama = new State(1, "Alabama");
        public static readonly State Arizona = new State(2, "Arizona");
        public static readonly State NewYork = new State(3, "New York");
        public static readonly State Illinois = new State(4, "Illinois");
        public static readonly State Texas = new State(5, "Texas");

        private State(int value, string displayName)
            : base(value, displayName)
        {
        }
    }
}
