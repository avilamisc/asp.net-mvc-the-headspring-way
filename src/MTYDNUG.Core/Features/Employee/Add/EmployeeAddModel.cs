﻿namespace MTYDNUG.Core.Features.Employee.Add
{
    using System.ComponentModel.DataAnnotations;
    using Enumerations;
    using MediatR;

    public class EmployeeAddModel : IRequest
    {
        public int Id { get; set; }

        public string Username { get; set; }

        public string LastName { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string DateOfBirth { get; set; }

        [Display(Name = "Address 1")]
        public string AddressAddress1 { get; set; }

        [Display(Name = "Address 2")]
        public string AddressAddress2 { get; set; }

        [Display(Name = "City")]
        public string AddressCity { get; set; }

        [Display(Name = "State")]
        public int AddressState { get; set; }

        [Display(Name = "Zip Code")]
        public string AddressZipCode { get; set; }
    }
}