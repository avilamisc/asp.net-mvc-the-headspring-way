﻿namespace MTYDNUG.Core.Features.Employee.Add
{
    using System.Linq;
    using FluentValidation;
    using Services;

    public class EmployeeAddValidator : AbstractValidator<EmployeeAddModel>
    {
        private readonly IEmployeeService _employeeService;

        public EmployeeAddValidator(IEmployeeService employeeService)
        {
            _employeeService = employeeService;

            RuleFor(x => x.Username).NotEmpty().EmailAddress().Must(BeUnique).WithMessage("The username {0} is already in use.", emp => emp.Username);
            RuleFor(x => x.FirstName).NotEmpty();
            RuleFor(x => x.LastName).NotEmpty();
            RuleFor(x => x.DateOfBirth).NotEmpty();
        }

        private bool BeUnique(string username)
        {
            return _employeeService.GetList().All(x => x.Username != username);
        }
    }
}