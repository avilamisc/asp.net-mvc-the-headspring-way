﻿namespace MTYDNUG.Core.Features.Employee.Add
{
    using AutoMapper;
    using Domain;

    public class MappingProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<Employee, EmployeeAddModel>()
                .ForMember(x => x.DateOfBirth, map => map.MapFrom(x => x.DateOfBirth.ToShortDateString()));
        }
    }
}