﻿namespace MTYDNUG.Core.Features.Employee.Edit
{
    using System;
    using Domain;
    using Enumerations;
    using MediatR;
    using Services;

    public class EmployeeEditHandler : RequestHandler<EmployeeEditModel>
    {
        private readonly IEmployeeService _employeeService;

        public EmployeeEditHandler(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        protected override void HandleCore(EmployeeEditModel message)
        {
            var employee = _employeeService.GetById(message.Id);

            employee.FirstName = message.FirstName;
            employee.MiddleName = message.MiddleName;
            employee.LastName = message.LastName;
            employee.DateOfBirth = DateTime.Parse(message.DateOfBirth);
            employee.Address.City = message.AddressCity;
            employee.Address.State = State.FromValue(message.AddressStateValue);
            employee.Address.Address1 = message.AddressAddress1;
            employee.Address.Address2 = message.AddressAddress2;
            employee.Address.ZipCode = message.AddressZipCode;

            _employeeService.SaveOrUpdate(employee);
        }
    }
}
