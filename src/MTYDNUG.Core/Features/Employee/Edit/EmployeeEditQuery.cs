﻿namespace MTYDNUG.Core.Features.Employee.Edit
{
    using MediatR;

    public class EmployeeEditQuery : IRequest<EmployeeEditModel>
    {
        public int Id { get; set; }
    }
}