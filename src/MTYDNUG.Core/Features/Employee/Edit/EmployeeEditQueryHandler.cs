﻿namespace MTYDNUG.Core.Features.Employee.Edit
{
    using System.Linq;
    using Infrastructure.Mapping;
    using MediatR;
    using Services;

    public class EmployeeEditQueryHandler : IRequestHandler<EmployeeEditQuery, EmployeeEditModel>
    {
        private readonly IEmployeeService _employeeService;

        public EmployeeEditQueryHandler(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        public EmployeeEditModel Handle(EmployeeEditQuery message)
        {
            var employeeModel = _employeeService.GetList().Single(emp => emp.Id == message.Id).Map().To<EmployeeEditModel>();
            return employeeModel;
        }
    }
}
