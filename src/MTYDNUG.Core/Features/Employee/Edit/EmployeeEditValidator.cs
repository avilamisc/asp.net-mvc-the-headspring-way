﻿namespace MTYDNUG.Core.Features.Employee.Edit
{
    using System.Linq;
    using FluentValidation;
    using Services;

    public class EmployeeEditValidator : AbstractValidator<EmployeeEditModel>
    {
        private readonly IEmployeeService _employeeService;

        public EmployeeEditValidator(IEmployeeService employeeService)
        {
            _employeeService = employeeService;

            RuleFor(x => x.Username).NotEmpty().EmailAddress().Must(BeUnique).WithMessage("The username {0} is already in use.", emp => emp.Username);
            RuleFor(x => x.FirstName).NotEmpty();
            RuleFor(x => x.LastName).NotEmpty();
            RuleFor(x => x.DateOfBirth).NotEmpty();
        }

        private bool BeUnique(EmployeeEditModel model, string username)
        {
            if (model.CurrentUsername == username)
            {
                return true;
            }

            return  _employeeService.GetList().All(x => x.Username != username);
        }
    }
}