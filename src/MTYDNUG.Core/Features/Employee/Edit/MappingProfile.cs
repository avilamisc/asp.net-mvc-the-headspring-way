﻿namespace MTYDNUG.Core.Features.Employee.Edit
{
    using AutoMapper;
    using Domain;

    public class MappingProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<Employee, EmployeeEditModel>()
                .ForMember(x => x.DateOfBirth, map => map.MapFrom(x => x.DateOfBirth.ToShortDateString()))
                .ForMember(x => x.CurrentUsername, map => map.MapFrom(x => x.Username));
        }
    }
}