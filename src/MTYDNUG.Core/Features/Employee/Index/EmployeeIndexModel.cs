﻿namespace MTYDNUG.Core.Features.Employee.Index
{
    using System.Collections.Generic;

    public class EmployeeIndexModel
    {
        public List<EmployeeModel> Employees { get; set; }

        public int NumberOfEmployees
        {
            get { return Employees.Count; }
        }

        public class EmployeeModel
        {
            public int Id { get; set; }

            public string Username { get; set; }

            public string LastName { get; set; }

            public string FirstName { get; set; }

            public string MiddleName { get; set; }

            public string DateOfBirth { get; set; }

            public string AddressAddress1 { get; set; }

            public string AddressAddress2 { get; set; }

            public string AddressCity { get; set; }

            public string AddressState { get; set; }

            public string AddressZipCode { get; set; }
        }
    }
}