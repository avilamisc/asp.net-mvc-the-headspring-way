﻿namespace MTYDNUG.Core.Features.Employee.Index
{
    using AutoMapper;
    using Domain;

    public class MappingProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<Employee, EmployeeIndexModel.EmployeeModel>()
                .ForMember(x => x.DateOfBirth, map => map.MapFrom(x => x.DateOfBirth.ToShortDateString()));
        }
    }
}