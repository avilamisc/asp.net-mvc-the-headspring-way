﻿namespace MTYDNUG.Core.Services
{
    using System.Collections.Generic;
    using Domain;

    public interface IEmployeeService
    {
        IEnumerable<Employee> GetList();

        Employee GetById(int id);

        void SaveOrUpdate(Employee employee);

        void Delete(Employee employee);
    }
}