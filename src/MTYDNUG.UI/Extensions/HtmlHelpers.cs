﻿using System;
using System.Collections.Generic;

namespace MTYDNUG.UI.Extensions
{
    using System.ComponentModel.DataAnnotations;
    using System.IO;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Text;
    using System.Web.Mvc;

    public static class HtmlHelpers
    {
        public static MvcHtmlString FriendlyLabelFor<T>(this HtmlHelper<T> helper, Expression<Func<T, object>> expression) where T : class
        {
            var fieldName = ExpressionHelper.GetExpressionText(expression);
            PropertyInfo prop = null;
            switch (expression.Body.NodeType)
            {
                case ExpressionType.MemberAccess:
                    prop = (PropertyInfo) ((MemberExpression) expression.Body).Member;
                    break;
                case ExpressionType.ConvertChecked:
                case ExpressionType.Convert:
                {
                    var ue = expression.Body as UnaryExpression;
                    var me = ((ue != null) ? ue.Operand : null) as MemberExpression;
                    prop = (PropertyInfo) (me).Member;
                }
                    break;
            }

            var displayName = !HasDisplayAttribute(prop) ? String.Join(" ", SplitPascalCase(fieldName)) : ReflectionExtensions.GetPropertyDisplayName(expression);

            return MvcHtmlString.Create(string.Format("<label for='{0}'>{1}</label>", fieldName, displayName));
        }

        private static bool HasDisplayAttribute(PropertyInfo property)
        {
            var hasIsIdentity = Attribute.IsDefined(property, typeof(DisplayAttribute));
            return hasIsIdentity;
        }

        private static IEnumerable<string> SplitPascalCase(string text)
        {
            var sb = new StringBuilder();
            using (var reader = new StringReader(text))
            {
                while (reader.Peek() != -1)
                {
                    char c = (char)reader.Read();
                    if (char.IsUpper(c) && sb.Length > 0)
                    {
                        yield return sb.ToString();
                        sb.Length = 0;
                    }

                    sb.Append(c);
                }
            }

            if (sb.Length > 0)
                yield return sb.ToString();
        }
    }
}