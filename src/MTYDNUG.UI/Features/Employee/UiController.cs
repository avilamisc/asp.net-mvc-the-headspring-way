﻿namespace MTYDNUG.UI.Features.Employee
{
    using System.Web.Mvc;
    using Core.Features.Employee.Add;
    using Core.Features.Employee.Edit;
    using Core.Features.Employee.Index;
    using Infrastructure;
    using MediatR;

    public class UiController : Controller
    {
        private readonly IMediator _mediator;

        public UiController(IMediator mediator)
        {
            _mediator = mediator;
        }

        public ActionResult Index(EmployeeIndexQuery employeeIndexQuery)
        {
            var model = _mediator.Send(employeeIndexQuery);
            return View(model);
        }

        public ActionResult Add()
        {
            var model = new EmployeeAddModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Add(EmployeeAddModel request)
        {
            _mediator.Send(request);
            return this.RedirectToActionJson("Index");
        }

        public ActionResult Edit(EmployeeEditQuery query)
        {
            var model = _mediator.Send(query);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(EmployeeEditModel request)
        {
            _mediator.Send(request);
            return this.RedirectToActionJson("Index");
        }
    }
}