﻿namespace MTYDNUG.UI.Features.Home
{
    using System.Web.Mvc;

    public class UiController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}