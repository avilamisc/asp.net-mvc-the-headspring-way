namespace MTYDNUG.UI.ModelBinding
{
    using System.ComponentModel;
    using System.Web.Mvc;

    public abstract class FilteredPropertyBinderBase : IFilteredPropertyBinder
    {
        protected static ValueProviderResult GetValue(ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor)
        {
            var prefix = "";

            if (!string.IsNullOrWhiteSpace(bindingContext.ModelName))
                prefix = bindingContext.ModelName + ".";

            return bindingContext.ValueProvider.GetValue(prefix + propertyDescriptor.Name);
        }

        public abstract bool ShouldBind(ControllerContext controllerContext, ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor);
        public abstract object GetPropertyValue(ControllerContext controllerContext, ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor);
    }
}