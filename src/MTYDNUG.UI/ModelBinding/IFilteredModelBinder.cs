namespace MTYDNUG.UI.ModelBinding
{
    using System.Web.Mvc;

    public interface IFilteredModelBinder
    {
        bool ShouldBind(ControllerContext controllerContext, ModelBindingContext bindingContext);
        object GetModelValue(ControllerContext controllerContext, ModelBindingContext bindingContext);
    }
}